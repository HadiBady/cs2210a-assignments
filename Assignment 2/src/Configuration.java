/**
 * the Configuration class allows us to store the configuration of the game board as an object
 * @author HADI BADAWI
 *
 */

public class Configuration {

	private String config; // stores the board's configuration in a string
	private int score; // stores the score assocated with the board
	
	/**
	 * Configuration() takes 2 parameters and stores their values
	 * @param config		this represents the string config of the board, will be stored into this.config
	 * @param score			this represents the score associated with that config, will be stored in this.score
	 * 						the constructor does not return anything
	 */
	public Configuration(String config, int score) {
		this.config = config;
		this.score = score;
	}
	
	/**
	 * getStringConfiguration() function returns a string
	 * @return			returns the string configuration stored in this class
	 */
	public String getStringConfiguration() {
		return this.config;
	}
	
	/**
	 * getScore() function returns an int
	 * @return			returns the score of this configuration that was stored
	 */
	public int getScore() {
		return this.score;
	}
}
