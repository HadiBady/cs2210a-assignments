/**
 * BoardGame Class represents the board game configuration for PlayGame.java
 * @author HADI BADAWI
 *
 */

public class BoardGame {
	
	private char[][] gameBoard; //represents the gameBoard configuration
	private int boardSize; // stores the size of the board
	
	final int TABLE_SIZE = 9973; // prime number chosen for the hash function
	
	/**
	 * BoardGame() initializes the BoardGame class, associating values from function to private 
	 * variables, and setting the table to 'g' in every column and row
	 * 
	 * @param board_size		represents the size of the board by the squared side
	 * @param empty_positions	represents the number of empty positions that the user specified when running 
	 * @param max_levels		represents the number of levels in the future that we can see in the future
	 */
	public BoardGame (int board_size, int empty_positions, int max_levels) {
		this.gameBoard = new char[board_size][board_size];
		this.boardSize = board_size;

		// setting table to 'g' characters (table is therefore empty, g represents") in every column and row to add them that way
		for (int i = 0; i< gameBoard.length; i++) {
			for (int j = 0; j< gameBoard.length; j++) {
				gameBoard[i][j] = 'g';
			}
			
		}
		
	}

	/**
	 * makeDictionary()			makes a new HashDictionary that is returned back to PlayGame.java
	 * @return				an empty hashDictionary with the size decided previously
	 */
	public HashDictionary makeDictionary() {
		HashDictionary gameDictionary = new HashDictionary(TABLE_SIZE);
		return gameDictionary;
		
	}
	
	// returns to you the score of your configuration
	public int isRepeatedConfig(HashDictionary dict) {
		String config = ConfigToString();
		return dict.getScore(config);
	}
	
	//put your configuration into the hash dictionary
	public void putConfig(HashDictionary dict, int score) {
		Configuration tempConfig = new Configuration (ConfigToString(), score);
			try {
				dict.put(tempConfig);
			} catch (DictionaryException e) {
				// no need to do anything, because we will only add items to the dictionary if they are not in the dictionary
				System.out.print(" ");
			}	
	}
	
	/**
	 * saveyPlay()		saves the play that has been made
	 * @param row		the row to save the play in
	 * @param col		the column to save the play in
	 * @param symbol	the symbol to store at that location
	 */
	public void savePlay(int row, int col, char symbol) {
		gameBoard[row][col] = symbol;
	}
	
	/**
	 * positionIsEmpty()	determines if the current position is empty
	 * @param row			the row to investigate
	 * @param col			the column to investigate
	 * @return				returns true if empty, otherwise false
	 */
	public boolean positionIsEmpty (int row, int col) {
		return gameBoard[row][col] == 'g';	}
	
	/**
	 * titleOfComputer()	determines if the current tile is the computer's tile
	 * @param row			the row to check
	 * @param col			the column to check
	 * @return				returns true if yes, otherwise false
	 */
	public boolean tileOfComputer (int row, int col) {
		return gameBoard[row][col] == 'o';
	}
	
	/**
	 * titleofHuman()		determines if the current tile is the human's tile
	 * @param row			the row to check
	 * @param col			the column to check
	 * @return				returns true if it is, otherwise returns false
	 */
	public boolean tileOfHuman(int row, int col) {
		return gameBoard[row][col] == 'b';
	}
	
	/**
	 * wins()			determines to see whether the symbol has won or lost
	 * @param symbol	represents the character of the player, o for computer and b for human
	 * @return			returns true if that symbol won, otherwise it returns false
	 */
	public boolean wins (char symbol) {
		
		// top to bottom checking
		for (int i = 0; i< boardSize; i++) {
			int counterTB = 0;
			for (int j = 0; j< boardSize; j++) {				
				if (gameBoard[i][j] == symbol) {
					counterTB++;					
				}
			}		
			if (counterTB == boardSize){
				return true;
			}
		
		}
				
		//left to right checking
		for (int i = 0; i< boardSize; i++) {
			int counterLR = 0;
			for (int j = 0; j < boardSize; j++) {				
				if (gameBoard[j][i] == symbol) {
					counterLR++;
				}
							
			}
					
			if (counterLR == boardSize){
				return true;
			}
		}	
				
		// Across from top left to bottom right and bottom left to upper right
		int counter = 0; // counter for top to bottom diagonal
		int counter2 = 0; // counter for bottom to top diagonal
		for (int i = 0; i< boardSize; i++) {
					
			if (gameBoard[i][i] == symbol) {
				counter++;
				}
			if (gameBoard[boardSize - 1 - i][i] == symbol) {
				counter2++;
				}
					
		}
		if (counter == boardSize || counter2 == boardSize){
			return true;
		}
		return false;
	}
	
	/**
	 * isDraw()					takes in 2 parameters and determines if this is a draw or not
	 * @param symbol			the symbol that is used to check if it is a draw
	 * @param empty_positions	this represents the number of empty positions that are supposed to remain on the board
	 * @return					true if it is a draw, otherwise, returns false
	 */
	public boolean isDraw(char symbol, int empty_positions) {
		String tempBoard = ConfigToString();
		
		int counter = 0; // will be used to determine if number of empty spots matches what was specified by empty_positions
		
		int boardSizeSquared = boardSize*boardSize;
		
		// counting the number of empty_spots
		for (int h = 0; h < boardSizeSquared; h++ ) {
			if (tempBoard.charAt(h) == 'g') {
				counter++;
			}
		}
		
		int inBoundCount = 0; // counter to determine for the else if case if there is truly a draw or not
		int notCount = 0; // counter to represent the number of spots you did not find yourself
		
		// if specified to have 0 empty spots, then it checks to make sure you ran out of spots and returns true
		if (empty_positions == 0) {
				if (counter == 0) {
					return true;
				}
		}
		
				
		// this checks every position to make sure to find the empty ones, once it finds any empty position, it will check to make sure that tiles
		// around it exist, and if they do inBoundCount is incremented,  then this checks if those are not itself. If this is true, then the 
		// notCount is also incremented.

		else if (empty_positions > 0 ) {
			for (int i = 0; i < boardSize; i++) {
					for (int j = 0; j < boardSize; j++) {
						if (positionIsEmpty(i,j)) {
							
							if (inBound(i-1, boardSize) && inBound(j-1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i-1][j-1] != symbol & !positionIsEmpty(i-1,j-1)) {
									notCount++;
								}
							}
							
							if (inBound(i-1, boardSize) && inBound(j, boardSize)) {
								inBoundCount++;
								if (gameBoard[i-1][j] != symbol & !positionIsEmpty(i-1,j)) {
									notCount++;
								}
							}
							
							if (inBound(i-1, boardSize) && inBound(j+1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i-1][j+1] != symbol & !positionIsEmpty(i-1,j+1)) {
									notCount++;
								}
							}
							
							if (inBound(i, boardSize) && inBound(j-1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i][j-1] != symbol & !positionIsEmpty(i,j-1)) {
									notCount++;
								}
							}
							
							if (inBound(i, boardSize) && inBound(j+1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i][j+1] != symbol & !positionIsEmpty(i,j+1)) {
									notCount++;
								}
							}
							
							if (inBound(i+1, boardSize) && inBound(j-1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i+1][j-1] != symbol & !positionIsEmpty(i+1,j-1)) {
									notCount++;
								}
							}
							
							if (inBound(i+1, boardSize) && inBound(j, boardSize)) {
								inBoundCount++;
								if (gameBoard[i+1][j] != symbol & !positionIsEmpty(i+1,j)) {
									notCount++;
								}
							}
							
							if (inBound(i+1, boardSize) && inBound(j+1, boardSize)) {
								inBoundCount++;
								if (gameBoard[i+1][j+1] != symbol & !positionIsEmpty(i+1,j+1)) {
									notCount++;
								}
							}
						}
						
							
			}
		}
		// if notCount matches the number of inBoundCount, then it is for sure that we have a draw
		if (notCount == inBoundCount) {
					return true;
				}
			}
		
		// returns false otherwise, thus the game is not a draw
		return false;
	}
	
	/**
	 * evalBoard() 				function takes in multiple conditions to calculate the score to by determining if this configuration wins, draws, losses, or no effect
	 * @param symbol			represents the current character to investigate, either b or o
	 * @param empty_positions	represents the number of empty positions that were pre-determined
	 * @return					returns an int that represents the score of the board
	 */
	public int evalBoard(char symbol, int empty_positions) {
		// condition to see if the computer wins
		if (wins('o')) {
			return 3;
		}
		//condition to see if the human wins
		else if (wins('b')) {
			return 0;
		}
		//condition to see if it is a draw
		else if (isDraw(symbol, empty_positions)) {
			return 2;
		}
		//condition for if the game is undecided
		else {
			return 1;
		}
	}
	
	/**
	 * ConfigToString()			is a helper function that turns the board configuration into a string
	 * @return
	 */
	private String ConfigToString() {
		String substring = "";
		
		for (int i = 0; i< boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				substring = substring + gameBoard[j][i];
			}	
		}
		return substring;
		
	}

	/**
	 * inBound 				helper function to determine if the index is not out of bounds
	 * @param index			this represents the index you are testing
	 * @param arrayLength	this is to know the length of the array
	 * @return				returns true if in bounds, otherwise, returns false
	 */
	private boolean inBound(int index, int arrayLength) {
		return (index >= 0) && (index < arrayLength);
	}

}