
public class Node {

	private Configuration head;
	private Node link;
	
	public Node(Configuration node) {
		this.head = node;
		this.link = null;
	}
	
	public void setNext(Configuration node) {
		Node newNode = new Node(node); 
		this.link = newNode;
	}
	
	public void setNext(Node node) {
		this.link = node;
	}
	
	public Node getNext() {
		return this.link;
	}
	
	public Configuration getHead() {
		return this.head;
	}
	
}
