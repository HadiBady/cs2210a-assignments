/**
 * the HashDictionary class implements the DictionaryADT, allowing for the removal, insertion, and obtaining the score for the configuration of each board
 * @author HADI BADAWI
 *
 */
public class HashDictionary implements DictionaryADT{
	private Node[] hashArray; // private array that stores the values that need to be inserted into the table
	private int size; // this stores the size of the table that will be created when the user initializes the HashDictioanry
	
	/**
	 * HashDictionary()		initializes the class and takes in 1 parameter
	 * @param size			this parameter is stored into the variable this.size, represents the size of the HashDictionary
	 */
	public HashDictionary(int size) {
		hashArray = new Node[size];
		this.size = size;
	}
	
	
	/**
	 * put() function inserts the Configuration type into the hashArray
	 * 
	 *  @param   data 		the configuration type that will be inserted into the hashArray
	 *  @throws  DictionaryException 	when you try to put the same data into the hashArray again
	 *  @return  0 if this is the first copy of data that is inserted into the hashArray and 1 if this isn't the first time
	 *  		 that data is attempted to be inserted into the hashArray
	 *  
	 */
	public int put(Configuration data) throws DictionaryException{
		
		int location = hashFunction(data.getStringConfiguration()); // calling the hash function to obtain the index for which to find the item in the hashArray
		
		// checks to see if there is something in that location to throw the exception if it matches the what we are trying to insert
		if (hashArray[location] != null) {
			if (hashArray[location].getHead().getStringConfiguration().equals(data.getStringConfiguration())) {
				throw new DictionaryException(data.getStringConfiguration());
			}
			
			// temporary variable to hold the first node to transfer through
			Node tempNode = hashArray[location];
			
			// trying to get to the end of the link, but at the same time, potentially a copy is already in the middle must be detected to through exception
			while (tempNode.getNext() != null) {
				if (tempNode.getHead().getStringConfiguration().equals(data.getStringConfiguration())) {
					throw new DictionaryException(data.getStringConfiguration());
				}
				// to increment through the linked nodes
				tempNode = tempNode.getNext();
			}
			
			//creating a new node since no matches have been found
			Node newNode = new Node(data);
			
			//setting the final node to our linked list
			tempNode.setNext(newNode);
	
			// returns 1 to indicate a collision occurred
			return 1;
		}
		// if there is nothing at the beginning at that hashArray
		else {
			Node tempNode = new Node(data);
			hashArray[location] = tempNode;
			return 0; // returns 0 if no collisions occurred
		}
	}

	/**
	 * remove() function removes the Configuration type from the hashArray
	 * 
	 *  @param   config 		the string representation of the configuration that will be removed from the hashArray
	 *  @throws  DictionaryException 	when you try to remove the an item not in the hashArray
	 *  
	 *  this method does not return anything
	 *  
	 */
	public void remove(String config) throws DictionaryException{
		
		
		boolean pass = false; // this is to determine if the item was successfully removed
		int location = hashFunction(config); // calling the hash function to obtain the index for which to find the item in the hashArray
		
		// checks to see if there is nothing in that location to throw the exception
		if (hashArray[location] == null) {
			throw new DictionaryException(config);
		}
		
		//this segment gets executed when there is something at that location and it happens to be what we are looking for to remove
		else if (hashArray[location].getHead().getStringConfiguration().equals(config)){
			// we take the current node, get the next node, then set the next node at the beginning
			Node currNode = hashArray[location];
			Node nextNode = currNode.getNext();
			
			hashArray[location] = nextNode;	
			pass = true; // indicates that we passed
		}
		
		// this segment gets executed when the item is not at that location
		else if (!hashArray[location].getHead().getStringConfiguration().equals(config)) {
			Node currNode = hashArray[location];
			Node nextNode = currNode.getNext();
			
			//making sure that there is something after the current node that is not null
			while (nextNode != null) {
				// if the next Node matches the config, then we need to remove that node
				if (nextNode.getHead().getStringConfiguration().equals(config)) {
					currNode.setNext(nextNode.getNext());
					pass = true; // indicates we passed
					break;
				}
				// this to keep moving forward when we find something
				currNode = currNode.getNext();
				nextNode = currNode.getNext();	
				}
			
			}
		// if we do not pass, then we throw an exception since the item was not found
		if (!pass) {
			throw new DictionaryException(config);
		}
	}

	/**
	 * getScore() function returns the score of a given configuration
	 * 
	 * @param config		this is the string representation for which when would like to know the score if present in hashArray
	 * @return int			-1 if the item is not present in the hashArray, but returns the score if the item is found in the hashArray 
	 */
	public int getScore(String config) {
		int location = hashFunction(config); // calling the hash function to obtain the index for which to find the item in the hashArray
		
		// if the location is empty at that index, then item is not present there
		if (hashArray[location] == null) {
			return -1;
		}

		// if the first node at that location contains the item that we require its score, then we simply return the score 
		if (hashArray[location].getHead().getStringConfiguration().equals(config)) {
			return hashArray[location].getHead().getScore();
		}
		// creating a node transfer through the potential linked nodes
		Node tempNode = hashArray[location];
		tempNode = tempNode.getNext();
		
		// only want to loop if something is there
		while (tempNode != null) {
			//checks if it found the item we are trying to obtain it's score, otherwise, sets to next node
			if (tempNode.getHead().getStringConfiguration().equals(config)){
				return tempNode.getHead().getScore();
			}
				tempNode = tempNode.getNext();	
			}
		return -1;
	
	}

	/**
	 * hashFunction() 	function returns the associated index in the hashArray using the polynomial method along with the Modulo to minimize collisions
	 * @param key		this represents the "key" (the string configuration of board) for the hashArray
	 * @return	int		this returns the corresponding location for which the items will be stored into the array		
	 */
	private int hashFunction(String key) {
		int val = (int) key.charAt(0) ; // this is the first character in the string key
		int k = key.length(); 
		int x = 33; // can also be 37,39, or 41, these are possible bases that could be used for the polynomial method
		int M = this.size; // this number should be the size of the table, needs to be prime
		
		// for loop to take the next character, add it to the product of x with current val, and modulo that by table size, until you get to the last itme
		for (int i = 1; i <= k-1 ; i++ ) {
			val = (val * x + (int) key.charAt(i)) % M;
		}
		
		return val;
	}
	
	
	
}
