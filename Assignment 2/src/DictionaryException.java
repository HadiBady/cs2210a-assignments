/**
 * DictionaryException Class defines the exception that is thrown when someone tries to preform an illegal action
 * @author HADI BADAWI
 *
 */


public class DictionaryException extends Exception{

	//prints indicating which string configuration was throwing the violation
	public DictionaryException (String string) {
		System.out.println("Dictionary Exception with entry: " + string );
	}
}
