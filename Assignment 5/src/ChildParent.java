
public class ChildParent {
	private GraphNode current;
	private GraphNode parent;
	private char busLine;
	
	public ChildParent(GraphNode current, GraphNode parent, char busLine) {
		this.current = current;
		this.parent = parent;
	}
	
	public char getBusLine() {
		return busLine;
	}

	public void setBusLine(char busLine) {
		this.busLine = busLine;
	}

	public GraphNode getCurrent() {
		return current;
	}
	public void setCurrent(GraphNode current) {
		this.current = current;
	}
	public GraphNode getParent() {
		return parent;
	}
	public void setParent(GraphNode parent) {
		this.parent = parent;
	}
	
}
