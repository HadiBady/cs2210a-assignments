/**
 * GraphNode class stores the index of a node (its name) and the mark (whether it is marked or not)
 * @author HADI BADAWI
 *
 */


public class GraphNode {
	
	int index = -1;// the indexes that will be stored will be greater than -1, temporary place holder
	boolean mark = false;// the default is to not be marked, hence false

	/**
	 * creates the GraphNode
	 * @param name		is the index of the GraphNode
	 */
	GraphNode(int name){
		this.index = name;
	}
	
	/**
	 * sets the mark of the graph node
	 * @param mark		is the boolean mark it will adjust to
	 */
	void setMark(boolean mark){
		this.mark = mark;
	}
	
	/**
	 * 
	 * @return		the mark of the node
	 */
	boolean getMark() {
		return this.mark;
	}
	
	/**
	 * 
	 * @return  the index of the node
	 */
	int getName() {
		return index;
	}
}
