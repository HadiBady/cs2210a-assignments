/**
 * GraphEdge class stores the GraphEdge which will be used by Graph
 * @author HADI BADAWI
 *
 */
public class GraphEdge {
	
	GraphNode startPoint, endPoint; // the start will be the first node, while the end will be the next node
	char busLine; // this will hold onto the character that represetns the busline
	
	/**
	 * constructor for GraphEdge, assigning each variable its corresponding value
	 * @param u		is the starting "node" 
	 * @param v		is the ending "node"
	 * @param busLine	is the busline character
	 */
	GraphEdge(GraphNode u, GraphNode v, char busLine){
		this.startPoint = u;
		this.endPoint = v;
		this.busLine = busLine;
	}
	
	/**
	 * 
	 * @return the starting point of an edge
	 */
	GraphNode firstEndpoint() {
			return this.startPoint;
	}

	/**
	 * 
	 * @return the ending point of an edge
	 */
	GraphNode secondEndpoint() {
		return this.endPoint;
	}
	
	/**
	 * 
	 * @return return the busline character
	 */
	char getBusLine() {
		return this.busLine;
		
	}
}
