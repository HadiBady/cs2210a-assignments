import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.Vector;

/**
 * BusLines creates a graph from a file, returns it to you, and allows you to
 * search for a possible to route to get from the start to the end if possible
 * 
 * @author HADI BADAWI
 *
 */
public class BusLines {
	private Graph graph; // this will store the graph
	private GraphNode start; // storing the start node of the graph
	private GraphNode destination; // storing the destination node of the graph

	// storing the fileLength, lineLength, and maximum bus changes
	private int fileLength = 0, lineLength = -1, maxBusChange = 0;

	// this 2d array will store the contents of the file
	private char[][] tempCity;
	// a stack to hold the possible solution to a graph
	private Stack tempStack = new Stack();
	// these hold the mapWidth and mapHeight, which will help us determine the
	// number of nodes to create
	private int mapWidth, mapHeight;

	/**
	 * the constructor analyzes the file, then calls a helper method to create the
	 * Graph
	 * 
	 * @param inputFile the file which will be analyzed to create the graph
	 * @throws MapException when an error occurs with creating the file, or if
	 *                      createGraph cannot create the graph
	 */
	public BusLines(String inputFile) throws MapException {
		readFile(inputFile); // calling helper method to create the 2D array from file contents
		createGraph(); // this will create the graph of the city
	}

	/**
	 * getGraph returns the graph if it exists, otherwise throws mapException
	 * 
	 * @return the graph
	 * @throws MapException when the graph could not be created
	 */
	public Graph getGraph() throws MapException {
		if (this.graph != null) {

			return this.graph;
		} else {
			throw new MapException("Graph could not be created");
		}

	}

	/**
	 * trip returns an iterator which you can traverse through to get the solution,
	 * otheriwse it returns null
	 * 
	 * @return the iterator of the stack which carries the solution
	 */
	public Iterator<GraphNode> trip() {

		try {
			// we attempt first by getting the list of all edges adjacent to the start
			Iterator<GraphEdge> edgeList = this.graph.incidentEdges(this.start);

			// tmpEdge will hold edges temporary which we will test to see if we find the
			// solution or not
			GraphEdge tmpEdge = null;
			// new GraphEdge(this.start, this.destination, ' ');

			// iterate through edgeList if there are potential ways to find a solution
			while (edgeList.hasNext()) {

				// getting the next edge in the iterator
				tmpEdge = edgeList.next();

				// will store the result if the solution was found
				boolean result = path(this.start, this.destination, this.maxBusChange, ' ');

				// if the solution was found, we simply return the iterator of that stack
				if (result) {
					return this.tempStack.iterator();
				}

			}
			// need to catch exceptions in case something is wrong, for debugging purpose
		} catch (Exception e) {
			System.out.println("Something is wrong");
		}
		// returning null if the solution could not find an answer
		return null;

	}

	/**
	 * function readFile takes in a filename which then adds the characters from the
	 * file and inserts it into the 2D array
	 * 
	 * @param filename contains the nodes to be created and stored in Graph
	 * @throws MapException when you cannot create the 2D array due to issues with
	 *                      the file
	 */

	private void readFile(String filename) throws MapException {
		Vector tempList = new Vector(); // this will hold all the characters temporary, until inserted into 2D array

		int i = 0; // will hold onto the line number from which we want to store the characters,
					// since line = 0
					// is where we get

		BufferedReader br = null; // creating a bufferedReader which will store the lines read from the text file

		int mapScale = 0;
		this.mapWidth = 0;
		this.mapHeight = 0;
		char[] tempChars;

		try {
			// setting up the text file to be read
			br = new BufferedReader(new FileReader(filename));
			String line = br.readLine(); // reading a line from the text file

			// to make sure while the line is not null to continue to do this
			while (line != null) {
				// if i is 0, then the line is holding the numbers to store, mapWidth,
				// mapheight, and maxBusChange
				if (i == 0) {
					// split the line by spaces
					String[] token = line.split(" ");

					// if you have invalid number of items on line 1
					if (token.length > 4 || token.length < 4) {
						throw new MapException("line 1, invalid amount of items");
					}
					// storing each value to corresponding variable
					mapScale = Integer.parseInt(token[0]); // this one does not aid in Graph constructing
					this.mapWidth = Integer.parseInt(token[1]); // # of vertical streets == max number of nodes possible
																// vertically
					this.mapHeight = Integer.parseInt(token[2]); // # of horizontal streets == max number of nodes
																	// possible horizontally
					this.maxBusChange = Integer.parseInt(token[3]); // maximum number of bus changes
				} else {

					// in this case, which is the rest of the file, we simply will add the array of
					// chars to a vector
					char[] token = line.toCharArray();
					tempList.add(token);

					// need to update lineLength, and we also make sure if lineLengths match
					// otherwise we need throw an exception
					if (lineLength == -1) {
						lineLength = token.length;
					} else if (lineLength != token.length) {
						throw new MapException("invalid line length");
					}

					/*
					 * else if (lineLength != token.length) { throw new MapException("File line "+ i
					 * +" does not match previous line lengths"); }
					 */
				}

				if (lineLength == -1 && i > 0) {
					throw new MapException("invalid line length");
				}

				i++; // increment i so that we get the number of "file lines" excluding the first
						// line
				line = br.readLine(); // read the next line
			}
			br.close(); // close the file

		} catch (FileNotFoundException e) {
			throw new MapException("File not found");
		} catch (IOException e) {
			throw new MapException("IO error, could not open the file.");
		}

		if (lineLength %2 == 0) {
			throw new MapException("incorrect number of characters for line length");
		}
		
		// storing the i to fileLength, but 1 less since the line number of actually
		// array is 1 less
		this.fileLength = i - 1;
		// making a graph as large as the number of nodes in the graph
		this.graph = new Graph(mapHeight * mapWidth);

		// making a 2d array as which fileLength as the first index and lineLength as
		// the second
		tempCity = new char[fileLength][lineLength];

		// to store the vectors into the tempCity 2d array
		for (int x = 0; x < (fileLength); x++) {
			tempCity[x] = (char[]) (tempList.toArray())[x];
		}

	}

	/**
	 * createGraph creates the graph, if needed it throws MapException
	 * @throws MapException		if it cannot create the graph
	 */
	private void createGraph() throws MapException {
		
		boolean skipLine = false; // condition to skip lines, so that we don't scan every line, when some scans
								  // are unnecessary
		int count = 0; // this holds the number for which node to check or create if non existant in the graph
		GraphNode tempNode; // will hold the node temporarly

		for (int x = 0; x < fileLength * lineLength; x += 2) {
			
			// here what is happening is this, we check if the node exists in the graph, if the graph
			// does not have it, we create a new node, tempNode, which we will store in the graph
			try {
				tempNode = this.graph.getNode(count);
			} catch (GraphException e) {
				tempNode = new GraphNode(count);
			}

			// checks to see if the current location in the 2d array has the start character
			if (tempCity[x / lineLength][x % lineLength] == 'S') {
				this.start = tempNode; // set this new node as the start
				// calls helper function to add this node to the graph and its edges
				createEdges(x / lineLength, x % lineLength, count, tempNode, tempCity[x / lineLength][x % lineLength]);
				count++; // to increment the node number

			// this checks to see if the current location in the 2d array has the destination character
			} else if (tempCity[x / lineLength][x % lineLength] == 'D') {
				this.destination = tempNode; // set this as the destination
				// call the helper function to add this to the node and its edges
				createEdges(x / lineLength, x % lineLength, count, tempNode, tempCity[x / lineLength][x % lineLength]);
				count++; // increment the node number
				
			// this case is for the general case, adding the node to the graph along with the edges	
			} else {
				createEdges(x / lineLength, x % lineLength, count, tempNode, '.');
				count++;
			}

			// if we reach the end of a line, then we must skip one line to have these conditions continue to be
			// true. so we make a flag that catches this
			if (((x + 1) % lineLength) == 0) {
				skipLine = true;
			}
			// skipLine is true, we must add the lineLength, and this only works because our 
			// class only accepts file with an odd length of line
			if (skipLine) {
				x += lineLength - 1;
				skipLine = false;
			}
		}
	}

	/**
	 * createEdges creates the edges for all the nodes that are to be created from the file
	 * @param x			the first index of the 2d array
	 * @param y			the second index of the 2d array
	 * @param oX		the node number currently inserted
	 * @param tempNode	the node to be added to the graph and its edges be created
	 * @param pBL		the character that could be a busline
	 * @throws MapException 
	 */
	private void createEdges(int x, int y, int oX, GraphNode tempNode, char pBL) throws MapException {
		// this hashMap simply stores whether we were able to create all four edge
		//
		// since the line number was skipped, what happens is this
		// 1        .
		// 2		a
		// 3  .  a  .   a  .
		// 4        a
		// 5        .
		//
		// if you look at the arragnment of the dots, you can notice that if we only look at the odd lines
		// what happens is this problem of making edges becomes easier and we only have to check
		// a node directly on top, node to the left and right, and the node directly under me
		//
		// the top node will have the index of 1, left (from readers perspective) will have index 2, right will have index 3
		// and the bottom will have index 4
		HashMap<Integer, GraphNode> potentialConnections = new HashMap<Integer, GraphNode>();
											
		// these are the temprorary variables to hold the nodes that reflect each index
		GraphNode tempGraphNode1 = null, tempGraphNode2 = null, tempGraphNode3 = null, tempGraphNode4 = null;

		// first check to make sure that we are in bounds of the array
		if (inBounds(x, fileLength) && inBounds(y, lineLength)) {
			
			// if the top node can potentially exist, then we will get it and store it in the hashmap
			if (inBounds(x - 2, fileLength) && inBounds(y, lineLength)) {
				
				// case to throw an exception if they put the S or D in an invalid place
				if (tempCity[x - 1][y] == 'D' && tempCity[x - 1][y] == 'S' && 
						!Character.isDigit(tempCity[x - 1][y]) && !Character.isLetter(tempCity[x - 1][y])) {
					throw new MapException("Invalid character" + tempCity[x - 1][y] +" for busline");
				}
				
				if (tempCity[x - 1][y] != ' ') {
					try {
						tempGraphNode1 = this.graph.getNode(oX - this.mapWidth);
					} catch (GraphException e) {
						tempGraphNode1 = new GraphNode(oX - this.mapWidth);
					}
					potentialConnections.put(1, tempGraphNode1);
				}
			}
			// if the left node exists, then we will get it and store it in the hashmap
			if (inBounds(x, fileLength) && inBounds(y - 2, lineLength)) {
				
				// case to throw an exception if they put the S or D in an invalid place
				if (tempCity[x][y - 1] == 'D' && tempCity[x][y - 1] == 'S' && 
						!Character.isDigit(tempCity[x][y-1]) && !Character.isLetter(tempCity[x][y-1])) {
					throw new MapException("Invalid character" + tempCity[x][y - 1] +" for busline");
				}
				
				if (tempCity[x][y - 1] != ' ') {
					try {
						tempGraphNode2 = this.graph.getNode(oX - 1);
					} catch (GraphException e) {
						tempGraphNode2 = new GraphNode(oX - 1);
					}
					potentialConnections.put(2, tempGraphNode2);
				}
			}
			
			// if the right node exists, then we will get it and store it in the hashmap
			if (inBounds(x, fileLength) && inBounds(y + 2, lineLength)) {
				
				// case to throw an exception if they put the S or D in an invalid place
				if (tempCity[x][y + 1] == 'D' && tempCity[x][y + 1] == 'S' && 
						!Character.isDigit(tempCity[x][y+1]) && !Character.isLetter(tempCity[x][y+1])) {
					throw new MapException("Invalid character" + tempCity[x][y + 1] +" for busline");
				}
				if (tempCity[x][y + 1] != ' ' ) {
					try {
						tempGraphNode3 = this.graph.getNode(oX + 1);
					} catch (GraphException e) {
						tempGraphNode3 = new GraphNode(oX + 1);
					}
					potentialConnections.put(3, tempGraphNode3);
				}
			}

			// if the bottom node, then we will get it and store it in the hashmap
			if (inBounds(x + 2, fileLength) && inBounds(y, lineLength)) {
				
				// case to throw an exception if they put the S or D in an invalid place
				if (tempCity[x+1][y] == 'D' && tempCity[x+1][y] == 'S' && 
						!Character.isDigit(tempCity[x + 1][y]) && !Character.isLetter(tempCity[x + 1][y])) {
					throw new MapException("Invalid character" + tempCity[x+1][y] +" for busline");
				}
				
				if (tempCity[x + 1][y] != ' ') {
					try {
						tempGraphNode4 = this.graph.getNode(oX + this.mapWidth);
					} catch (GraphException e) {
						tempGraphNode4 = new GraphNode(oX + this.mapWidth);
					}
					potentialConnections.put(4, tempGraphNode4);
				}
				
			}
		}

		// now we check to see if the hashmap contains that key which correspondes to a node
		// if it does, then we create a node, and if the node already exists, we catch it and
		// ignore it
		if (potentialConnections.containsKey(1)) {
			try {		
				this.graph.insertEdge(tempNode, tempGraphNode1, tempCity[x - 1][y]);
			} catch (GraphException e) {

			}
		}

		// now we check to see if the hashmap contains that key which correspondes to a node
		// if it does, then we create a node, and if the node already exists, we catch it and
		// ignore it
		if (potentialConnections.containsKey(2)) {
			try {
				this.graph.insertEdge(tempNode, tempGraphNode2, tempCity[x][y - 1]);

			} catch (GraphException e) {

			}
		}

		// now we check to see if the hashmap contains that key which correspondes to a node
		// if it does, then we create a node, and if the node already exists, we catch it and
		// ignore it
		if (potentialConnections.containsKey(3)) {

			try {
				this.graph.insertEdge(tempNode, tempGraphNode3, tempCity[x][y + 1]);

			} catch (GraphException e) {

			}
		}
		
		// now we check to see if the hashmap contains that key which correspondes to a node
		// if it does, then we create a node, and if the node already exists, we catch it and
		// ignore it
		if (potentialConnections.containsKey(4)) {

			try {
				this.graph.insertEdge(tempNode, tempGraphNode4, tempCity[x + 1][y]);

			}

			catch (GraphException e) {

			}
		}

	}

	/**
	 * inBounds checks to make sure that the index is within the size of given parameter, used in the arrays
	 * @param index			is the index to compare with
	 * @param maxIndex		the maximum index which should not be equal to index
	 * @return				whether index is less than maxIndex, and greater or equal to 0
	 */
	private boolean inBounds(int index, int maxIndex) {
		return index >= 0 && maxIndex > index;
	}

	/**
	 *  path is a helper method to trip() which will find the path from current node to a destination node
	 * @param current		is the current nodes which the helper method is trying to find a path to the destination
	 * @param target		is the target node (destination) to which we want to reach
	 * @param maxChange		the maximum number of bus changes
	 * @param lastLine		is the last busLine which we were on
	 * @return				true or false depending on if it found a solution or not
	 */
	private boolean path(GraphNode current, GraphNode target, int maxChange, char lastLine) {
		
		// we push the current node on the stack
		tempStack.push(current);

		// if we reach the end, we return true
		if (current == target) {
			return true;
		}

		// otherwise, we try to find the answer
		else {

			
			try {
				// set current node to true, indicating we visited this node
				current.setMark(true);

				// grab the list of edges to current node
				Iterator<GraphEdge> tempEdge = this.graph.incidentEdges(current);

				// only want to continue if there are edges left to check
				while (tempEdge.hasNext()) {

					// get the next edge
					GraphEdge currentEdge = tempEdge.next();

					// grab the end of the tempEdge
					GraphNode potNextCurrent = currentEdge.secondEndpoint();

					// if the end point of the edge is not marked, then we want to check it out
					if (!potNextCurrent.getMark()) {

						// because we call this function with a lastline = ' ', then we want
						// to make sure to grab the busline to be able to due future comparisons of 
						// bus lines
						if (current == this.start) {
							lastLine = currentEdge.getBusLine();
						}

						// if the buslines match, then we call ourselves recursively
						if (currentEdge.getBusLine() == (lastLine)) {
							// and if we  are true, then we return true ==> meaning we found a solution
							if (path(potNextCurrent, target, maxChange, lastLine)) {
								return true;
							}

							// that is not the case that we have same buslines
						} else {
							// then we see if we have any potential bus changes
							// if so then we change, otherwise we don't enter this part
							if (maxChange > 0) {
								// first decrease the line count
								maxChange--;

								// call ourselves and see if we get true with updated busLine
								if (path(potNextCurrent, target, maxChange, currentEdge.getBusLine())) {
									return true;
								}
								
								// if not, then we return the maxChange back to what it was, by increasing the counter by 1
								maxChange++;

							}

						}
					}

				}
				
				// this is in the case we fail, we set this node back to false
				current.setMark(false);
				
				// and we pop ourselves off of the stack
				tempStack.pop();

			}
			
			// in case something goes wrong, we only print a statement
			catch (Exception e) {
				System.out.println("Something is wrong");
			}

		}
		// return false since at this point, we did not find a solution
		return false;
	}
}
