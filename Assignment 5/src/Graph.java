import java.util.Vector;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The Graph Class implements GraphADT using a GraphNode array for the nodes and
 * a Vector array for the adjaceny array
 * 
 * @author HADI BADAWI
 *
 */
public class Graph implements GraphADT {
	private GraphNode[] nodeArray; // stores the nodes in the graph
	private Vector[] adjacenyArray; // stores the edges in an adjaceny array
	private int n; // this keeps track of the number of nodes in the graph

	/**
	 * constructor for Graph
	 * 
	 * @param n is the number of nodes created for the graph
	 */
	public Graph(int n) {
		this.nodeArray = new GraphNode[n]; // making the size of the graph array equal to the number of nodes
		this.adjacenyArray = new Vector[n]; // making the size of the adjaceny array equal to the number of nodes

		// this for loop creates a new graph node for each index of the nodeArray
		for (int i = 0; i < n; i++) {
			this.nodeArray[i] = new GraphNode(i);
		}
		// storing the number of nodes in the graph
		this.n = n;
	}

	/**
	 * 
	 * inserts an edge into the graph if possible, otherwise throws an expcetion
	 * 
	 * @param u       is the "starting" node
	 * @param v       is the "ending" node
	 * @param busLine is the busLine which connects the 2 together
	 * 
	 * @throws GraphException when you have either node is null, when a nodes is not
	 *                        in the graph (index) or when you have an edge that
	 *                        already exists
	 */
	public void insertEdge(GraphNode u, GraphNode v, char busLine) throws GraphException {
		nullChecker(u, v); // throws GraphException if either node is null

		// these temporary variables are to make sure that the nodes actually exist in
		// the graph
		// they throw the exception if either on does not exist
		GraphNode tempNode = getNode(u.getName());
		GraphNode tempNode2 = getNode(v.getName());
		GraphEdge tempEdge = null;

		// trying to see if we already have an edge that already exists
		// if so, we will deal with it later, if not, we don't want
		// this function to throw an exception so we simply catch it
		try {
			tempEdge = getEdge(u, v);
		} catch (Exception e) {
			// this means that the edge does not exist
			// don't want to do anything
		}

		// if the edge exists, then we must throw an exception
		if (tempEdge != null) {
			throw new GraphException("Edge already exists");
		}

		// we obtain the vectors that is storing each adjaceny list for both nodes u and
		// v
		Vector tempVecU = adjacenyArray[u.getName()];
		Vector tempVecV = adjacenyArray[v.getName()];

		// we make a new edge for u to v and v to u with the same bus line
		GraphEdge tempEdgeU = new GraphEdge(u, v, busLine);
		GraphEdge tempEdgeV = new GraphEdge(v, u, busLine);

		// if graph node u does not have a vector already stored for the adjaceny list,
		// we create a new one
		if (tempVecU == null) {
			Vector tempVec = new Vector();
			tempVecU = tempVec;
		}
		// we simply add that edge to that vector
		tempVecU.add(tempEdgeU);

		// if the graph node v does not have vector already stored for the adjaceny
		// list, we create a new one
		if (tempVecV == null) {
			Vector tempVec2 = new Vector();
			tempVecV = tempVec2;
		}

		// we simply add that edge to that vector
		tempVecV.add(tempEdgeV);

		// now the previous vectors are overwritten with new modified vectors
		adjacenyArray[u.getName()] = tempVecU;
		adjacenyArray[v.getName()] = tempVecV;
	}

	/**
	 * getNode retrieves the node from the graph, otherwise it throws an exception
	 * 
	 * @param name is the int that references the node in the graph
	 * @throws GraphException if the nodes is not in the Graph
	 * 
	 */
	public GraphNode getNode(int name) throws GraphException {
		// checks to make sure that the number is within bounds ...
		if (inBounds(name, this.n)) {
			return nodeArray[name];
		}

		// otherwise, we throw the exception
		else {
			throw new GraphException("Vertex is not in the Graph");
		}
	}

	/**
	 * IncidentEdges returns a list of all edges incidient with node u or if there
	 * aren't any null, otherwise throws an exception
	 *
	 * @param u which is the node which we want the incident edges
	 * @return an iterator of edges, otherwise null
	 * @throws GraphException if nodes is not in the graph
	 */
	public Iterator<GraphEdge> incidentEdges(GraphNode u) throws GraphException {

		GraphNode tempNode = getNode(u.getName()); // throws the GraphException if the node is not in graph

		// if the adjacenyArray is empty, then we know, based on how this class was
		// implemented, that
		// we should return null
		if (adjacenyArray[u.getName()] == null) {
			return null;
		}

		// making a temporary edgeArray, which will store the adjacent nodes
		GraphEdge[] edgeArray = new GraphEdge[adjacenyArray[u.getName()].size()];
		// copying the adjaceny vector into the edgeArray
		adjacenyArray[u.getName()].copyInto((GraphEdge[]) edgeArray);

		// this array edgeList will be used since GraphEdge[] cannot have iterator
		// invoked on it
		ArrayList<GraphEdge> edgeList = new ArrayList<GraphEdge>();

		// copying the contents of edgeArray to edgeList
		for (int i = 0; i < edgeArray.length; i++) {
			edgeList.add(edgeArray[i]);
		}

		// creating the iterator version of edgeList
		Iterator<GraphEdge> iter = edgeList.iterator();
		// returning the iterator of edgeList
		return iter;
	}

	/**
	 * getEdge returns if an edge already exists in the graph, otherwise it throws
	 * an exception
	 * 
	 * @param u is the first point of that edge
	 * @param v is the second point of that edge
	 * @return the edge which connects u to v
	 * @throws GraphException if that edge does not exist, or nodes do not exist, or
	 *                        if they are null
	 */
	public GraphEdge getEdge(GraphNode u, GraphNode v) throws GraphException {
		// checks if nodes are null, throws exception if they are
		nullChecker(u, v);

		// check to make sure that the nodes are in the graph
		GraphNode tempNode = getNode(u.getName());
		GraphNode tempNode2 = getNode(v.getName());

		// grabs the adjacenyArray of u, since adjaceny Array of u will have the edge of
		// u to v
		Vector tempVec = adjacenyArray[u.getName()];

		// if the vector stored in that adjaceny array is not null, then we know that
		// it might be in it
		if (tempVec != null) {
			// we call incidentEdges to return an iterator of that vector
			Iterator<GraphEdge> tempIter = incidentEdges(u);
			// declaring the tempEdge, which will hold onto the edge
			GraphEdge tempEdge;

			// while loop to continue only when there are edges to search
			while (tempIter.hasNext()) {
				// incrementing through the iterator
				tempEdge = tempIter.next();
				// if we have found the edge, which has u and v in it, then we return it
				if (tempEdge.firstEndpoint() == u && tempEdge.secondEndpoint() == v) {
					return tempEdge;
				}
			}
		}

		// in the case where we did not find it, we throw the exception
		throw new GraphException("the vertexes " + u.getName() + " and " + v.getName() + " do not have an edge");
	}

	/**
	 * areAdjacent takes returns whether or not 2 nodes are adjacent, and throws exception when required to
	 * 
	 * @param	u	is the start point of the edge
	 * @param	v	is the end point of the edge
	 * @throws	GraphException		when nodes do not exist or when they are not in the graph
	 * @return		boolean for if they are adjacent or not
	 */
	public boolean areAdjacent(GraphNode u, GraphNode v) throws GraphException {
		// null nodes checker
		nullChecker(u, v);
		
		// temporary Edge holder
		GraphEdge tempEdge;

		// checking if the nodes are in the graph, throw the exception if that is the case
		getNode(u.getName());
		getNode(v.getName());

		// assuming both have an edge the exists
		boolean uv = true, vu = true;

		// for debugging purposes, i check both the edge for u to v and v to u
		
		
		// first check to see if edge exists between u and v
		try {
			tempEdge = getEdge(u, v);
		} 
		// if not, we simply return false
		catch (GraphException e) {
			return false;
		}

		// now check if edge exists between v and u
		try {
			tempEdge = getEdge(v, u);
		} 
		// if not, then return false
		catch (GraphException e) {
			return false;
		}
		
		

		// otherwise return true
		return true;

	}

	/**
	 * checks to see if an index is in bounds
	 * @param name		is the int representing of a node
	 * @param n			is the size for which they want to check, which is equal to this.n
	 * @return			if node is less than the index
	 */
	private boolean inBounds(int name, int n) {
		return (name >= 0 && name < n);
	}

	/**
	 * nullChecker checks to see if either node exists or not
	 * @param u		the "first" endpoint or node
	 * @param v		the "second" endpoint or node
	 * @throws GraphException		if either of them do not exist
	 */
	private void nullChecker(GraphNode u, GraphNode v) throws GraphException {
		if (u == null && v == null) {
			throw new GraphException("both graph nodes do not exist");
		}
		if (u == null) {
			throw new GraphException("the first graph node does not exist");
		}
		if (v == null) {
			throw new GraphException("the second graph node does not exist");
		}
	}
}
