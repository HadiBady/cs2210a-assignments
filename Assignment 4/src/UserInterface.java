/**
 * @author hadi
 *  The Class UserInterface allows one to import a file which can have any of the 2 types of data
 *  and interact with that data easily through command line
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UserInterface {
	
	public static void main(String[] args) {
		// creating an OrderedDictionary which will store the data
		OrderedDictionary ourDict = new OrderedDictionary();
		
		// calling the helper method to turn the text file into data stored in ourDict
		readFile(args[0], ourDict);
		
		// keyboard reader to take the input from the keyboard as a string
		StringReader keyboard = new StringReader();
		
		// escape is to tell when to leave the infinity loo
		boolean escape = false;
		
		System.out.println("Welcome to UserInterface");
		
		// condition to continue asking for user input
		while (!escape) {
			String line = keyboard.read("Enter next command: ");
		
			// split the lines by spaces
			String[] tokens = line.split(" ");
			
			// check to perform the operations for the get option
			if (tokens[0].equals("get")) {
				// make sure that the user does not input too many items into the command, only the required ammount
				if (tokens.length  ==2) {
					// the word that the user wishes to get
					String tempWord = tokens[1];
			
					// due to the way the get operation for OrderedDictionary is defined,
					// we must test to see if each type exists within the ourDict
					Record potentialText = ourDict.get(new Pair(tempWord.toLowerCase(), "text"));
					Record potentialAudio = ourDict.get(new Pair(tempWord.toLowerCase(), "audio"));
					Record potentialImage = ourDict.get(new Pair(tempWord.toLowerCase(), "image"));
					
					// if text exists, then we must print the text
					if (potentialText != null) {
						System.out.println(potentialText.getData());
					}
					//if the image exists, then we display the picture
					if (potentialImage != null) {
						// create a new PictureViewer to display the image
						PictureViewer viewer = new PictureViewer();
						// we try to display the picture
						try {
							viewer.show(potentialImage.getData());
						} 
						// and catch exception and we display a special error message
						catch (MultimediaException e) {
							System.out.println("Multimedia error: error with "+ potentialImage.getData());
						}
					}
					// if the audio exists, then we play the audio file
					if (potentialAudio != null) {
						// create a SoundPlayer to play the audio file
						SoundPlayer tempAudio = new SoundPlayer();
						// try to play the audio file
						try {
							tempAudio.play(potentialAudio.getData());
						} 
						// catch the exception if the multimedia exception is thrown 
						catch (MultimediaException e) {
							System.out.println("Multimedia error: error with "+ potentialAudio.getData());
						}
						
					}
					
					// in the case where the item is not in ourDict
					if (potentialText == null && potentialAudio == null && potentialImage == null) {
						// print to the user that we could not find the item in ourDict
						System.out.println("The word "+ tokens[1]+ " is not in the ordered dictionary.");
						// get the predecessor for our item, and to do that, we get the predecessor of 
						// this item with the type audio because audio is smaller than text and image
						Record predecessor1 = ourDict.predecessor(new Pair(tokens[1], "audio"));
						// get the successor for our item, and to do that, we get the successor of
						// this item with the type text because text is larger than image and audio
						Record successor1 = ourDict.successor(new Pair(tokens[1], "text"));
						
						// if the predecessor1 exists, we display the word
						if (predecessor1 != null) {
							System.out.println("Preceding word: " + predecessor1.getKey().getWord());
						}
						// if not, then we display a blank
						else {
							System.out.println("Preceding word: ");
						}
						// if successor1 exists, we display the word
						if (successor1 != null) {
							System.out.println("Following word: " + successor1.getKey().getWord());
						}
						// if not, we display a blank
						else {
							System.out.println("Following word: ");
						}
					}
					// this continue is to not display that feedback message in case the user inputs in the wrong
					// amoung of required items, and for the error if they put an invalid input
					continue;
			}
			// give feedback to the user that they inputed in the wrong amount of items for this command
			System.out.println("Insufficient / Access information entered to get the desired word.\nEnter first \"get\" then the desired word to obtain.");	
		}
		
		// check to decide if to perform the delete operation
		if (tokens[0].equals("delete")) {
			// check to make sure that you inputed the correct number of operations
			if (tokens.length == 3) {
				
				// try to remove the item from ourDict
				try {
					ourDict.remove(new Pair(tokens[1], tokens[2]));
				}
				// catch exception which is the case where the item is not in ourDict
				catch (DictionaryException e) {
					System.out.println("No record in the ordered dictionary has key (" + tokens[1] + ", " + tokens[2]+ ")");
				}
				// to skip the insufficient amount info and the invalid input message 
				continue;
			}
			
			// this is the message when tye input the incorrect amount of information
			System.out.println("Insufficient / Access information entered to delete the desired word.\n"
					+ "Enter first \"delete\" then the word you want to delete then the type you wish to remove.");
			
		}
		
		// condition to make sure that you can perform the put operation
		if (tokens[0].equals("put")) {
			// making sure you did not enter in the incorrect number of operations
			if (tokens.length == 4) {	
				// checking to make sure that the type is the correct one
				if (tokens[2] == "text" || tokens[2] == "audio" || tokens[2] == "image") {
					// making a new record with the correct amount of information
					Record newRec = new Record (new Pair(tokens[1], tokens[2]), tokens[3]);
					// try to insert the item into ourDict
					try {
						ourDict.put(newRec);
					}
					// if the item is already present, then catch the Dictionary Exception and print
					// a message to indicate an error with the insertion, as the item is already present
					catch (DictionaryException e) {
						System.out.println("A record with the given key ("+ tokens[1] + ", " + tokens[2] + ") is already in the ordered dictionary.");
					}
				}
				// case where the type inputed is not valid thus we print a message to tell the user the 
				// acceptable types and their type the user entered
				else {
					System.out.println("the type "+ tokens[2] + " cannot be inserted into this dictioionary.");
					System.out.println("The acceptable types are \"image\", \"audio\", and \"text\".");	
				}
			// to skip the insufficient amount info and the invalid input message 
			continue;
			}
			// print this message when the invalid amount of info is entered
			System.out.println("Insufficient / Access information to put the desired word.");		
		}
		
		// checking to see if the list operation will be performed
		if (tokens[0].equals("list")) {
			// checking to see if user entered in the correct number of inputs
			if (tokens.length == 2) {
				/*
				 * 2 ways to traverse through the OrderedDictionary:
				 * 1. you traverse through the tree by getting the smallest node and ask for successor until you get to largest
				 * 	  node's child, which will be a null type
				 * 2. you traverse through the tree by getting the largest node and ask for the precessesor until you get to
				 * 	  to the smallest node's child, the null node, then store this into an array and then print it in the correct order
				 * 
				 * this UserInterface will use the first method
				 */
				
				// getting the smallest item in ourDict
				Record start = ourDict.smallest();
				// counter to count how many items are printed to the screen
				int n = 0;
			
				// if ourDict is not empty, then start is not null, and to exit when you reach the null node
				while (start != null) {
					// check to make sure that you only compare prefix equal or short than the work
					// as a prefix can't be a prefix to a work if it is larger than the word it is being
					// compared to 
					if (tokens[1].length()<=start.getKey().getWord().length()) {
						// if the substring of the word is equal to the prefix, then will print the word
						if (tokens[1].equals(start.getKey().getWord().substring(0, tokens[1].length()))){
							// if this is the first word printed, then print the word with the new line 
							// character
							if (n == 0) {
								System.out.print(start.getKey().getWord());
								n++;
							}
							// in this case, this is not the first word, thus print a comma before
							// printing the next word
							else {
								System.out.print(", " + start.getKey().getWord());
								n++;
							}
						}
					}
					// update start to get to the next item in ourDict
					start = ourDict.successor(start.getKey());
				}
				// finally print the newline character
				System.out.print("\n");
			
				// message to print if no words were found in the ordered dictioanry with that prefix
				if (n == 0) {
					System.out.println("No words in the ordered dictionary start with prefix "+ tokens[1]);
				}
				
				// continue to skip error message and insufficient access meesage
				continue;
			}
			// insufficient / access message
			System.out.println("Insufficient / Access information to list the desired word(s) with that prefix.\n"
					+ "First enter \"list\" then enter the prefix.");
			
			
		}
		
		// check to determine to perform the operation for smallest
		if (tokens[0].equals("smallest")) {
			// ensure that correct amount of info is entered
			if (tokens.length == 1) {
				// getting the smallest item from ourDict
				Record smallest = ourDict.smallest();
			
				// if it is not null, hence, ourDict is not empty, we print the item
				if (smallest != null) {
					System.out.println(smallest.getKey().getWord()+","+smallest.getKey().getType()+","+smallest.getData()+".");
				
				}
				// if it is null, then ourDict is empty
				else {
					System.out.println("It seems that your dictionary is empty, thus there is no smallest item.");
				}
				// to avoid the invalid input and too much message
				continue;
			}
			// message to indicate to user that too many items were entered.
			System.out.println("Too much information to get the smallest \"word\".\n"
					+ "Enter only \"smallest\" to get the smallest word");
			
		}
		
		// to determine if the largest option will be run
		if (tokens[0].equals("largest")) {
			// check to make sure that the correct amount of info is entered by the user
			if (tokens.length == 1) {
				// get the largest item from ourDict
				Record largest = ourDict.largest();
			
				// when the largest is not null, then print the largest item
				if (largest != null) {
					System.out.println(largest.getKey().getWord()+","+largest.getKey().getType()+","+largest.getData()+".");
				}
				// when there are no items in ourDict, thus largest is equal to null
				else {
					System.out.println("It seems that your dictionary is empty, thus there is no largest item.");
				}
				// to avoid the invalid input and too much info message
				continue;
			}
			
			// message to indicate to user that too many items where entered
			System.out.println("Too much information to get the largest \"word\".");
		}
	
		// determine if the finish option will be selected
		if (tokens[0].equals("finish")) {
			if (tokens.length  == 1) {
				// now that the user wants to terminate the program, we set escape to true
				escape = true;
				// notify the user that the program is shutting down
				System.out.println("Exitting the program.");
				// avoid the try again and too many items message
				continue;
			}
			System.out.println("Too many items enterd to terminate the program.\n"
					+ "Type \"finish\" to terminate the program");
		}
		
		// message to indicate to the user that they can try again, it occurs when they typed in 
		// the incorrect number of things to perform the required task
		System.out.println("Please try again.");
		}
	}
	

	/**
	 * function readFile takes in a filename and an OrderedDictionary which then adds the items from 
	 * 					the file and inserts it into the OrderedDictionary
	 * @param filename		contains the nodes to be created and stored in OrderedDictionary
	 * @param ourDict		contains that nodes that were stored from the text file
	 */

	private static void readFile(String filename, OrderedDictionary ourDict) {
		int i = 0; // keeps track of whether the line is odd or even, to indicate whether it is a word or the actual data
				   // even ints represent words, and odd ints represent data
		BufferedReader br = null; // creating a bufferedReader which will store the lines read from the text file
			try {
				// setting up the text file to be read
				br = new BufferedReader(new FileReader(filename));
			    String line = br.readLine(); // reading a line from the text file
			    
			    String word = null; // will store the word to be stored
			    
			    // to make sure while the line is not null to continue to do this
			    while (line != null) {
			    	// if i is even, then the line is holding the word to be added to ourDict
			    	if (i%2 == 0) {
			    		word = line;
			    	}
			    	// i is then even, which means now we are analyzing the data
			    	else {
			    		// tokenize by the period, which will help us to determine if an item is an audio type
			    		// or an image type, or a text type
			    		String[] token = line.split("\\.");
			    		// creating variables to store the type and data
			    	    String type = " ", data = " ";
			    	    
			    	    // if you tokenize, and you end up with a length of 1, then there is no period, or if there is,
			    	    // there is nothing on the right of that preiod, hence a text type
			    	    if (token.length == 1) {
			    	    	type = "text";
			    	    }
			    	    // if you tokenize and the length is definitly greater than 1, then we need to see if it is a 
			    	    // type of image or audio, and we know the type by looking at token[1] since it represents the value
			    	    // on the right of the period
			    	    else {
			    	    	if (token[1].equals("jpg") || token[1].equals("gif")) {type = "image";} // image type with these extensions
			    	    	if (token[1].equals("wav") || token[1].equals("mid")) {type = "audio";} // audio type with these extensions
			    	    }
			    	    
			    	    // store the line as data, since this represents that data to be stored
			    	    data = line;
			    	    // making a new record that will store a pair with word and type as the key, and data as the data for that record
			    	    Record recToInsert = new Record (new Pair(word, type), data);
			    	    
			    	    // try to insert the item into the ourDict
			    	   	try {
			    	   		ourDict.put(recToInsert);
			    	   	}
			    	   	// catch if the exception is thrown
			    	   	catch (DictionaryException e) {
			    	   		System.out.println("Error trying to insert item" + word + " " + type + " " + data + ".");
			    	    	break;
			    	   	}
			    	}
			    	i++; // increment i so that we get the even odd pattern
			        line = br.readLine(); // read the next line
			    }
				    
			} 
			catch (FileNotFoundException e) {
				System.out.println("File not found.");
			} 
			catch (IOException e) {
				System.out.println("IO error, could not open the file.");
			}
			// try closing the file after using it
			try {
				br.close();
			} 
			catch (IOException e) {
				System.out.println("IO Error, could not close file.");
			}	
	}		

}
