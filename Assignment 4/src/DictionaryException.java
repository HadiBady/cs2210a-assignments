/**
 * DictionaryException Class defines the exception that is thrown when someone tries to perform an illegal action
 * @author HADI BADAWI
 *
 */
public class DictionaryException extends Exception{
	//prints indicating which operation was throwing the violation
	public DictionaryException (String string) {
		System.out.println("Dictionary Exception with operation: " + string );
	}
}

