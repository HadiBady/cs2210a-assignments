import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class traverseTester {

	public static void main(String[] args) {
		
		
		StringReader keyboard = new StringReader();
		OrderedDictionary ourDict = new OrderedDictionary();
		
		readFile(args[0], ourDict);
		
		String line = keyboard.read("Enter next command: ");
		
		String[] tokens = line.split(" ");
		
		// make a check to make sure that the people know that you do not randomly do the commands when you input the wrong values
					Record start = ourDict.smallest();
					int n = 0;
					
					while (start != null) {
								if (n == 0) {
									System.out.print(start.getKey().getWord());
									n++;
								}
								else {
									System.out.print(", " + start.getKey().getWord());
									n++;
								}
						start = ourDict.successor(start.getKey());
					}
					System.out.print("\n");
					
					if (n == 0) {
						System.out.println("No words in the ordered dictionary start with the prefix "+ tokens[1]);
					}
					
	}
	
	private static void readFile(String filename, OrderedDictionary ourDict) {
		//int status = FAILURE;
				int i = 0; // Increment to ensure that the first time through the files, it skips the headers
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(filename));
				    String line = br.readLine();
				    
				    String word = null;
				    
				    while (line != null) {
				    	if (i%2 == 0) {
				    		word = line;
				    	}
				    	else {
				    		String[] token = line.split("\\.");
				    	    String type = " ", data = " ";
				    	    if (!(token.length > 1)) {
				    	    	type = "text";
				    	    }
				    	    else {
				    	    	if (token[1].equals("jpg") || token[1].equals("gif")) {type = "image";}
				    	    	if (token[1].equals("wav") || token[1].equals("mid")) {type = "audio";}
				    	    }
				    	    
				    	    
				    	    data = line;
				    	    Record recToInsert = new Record (new Pair(word, type), data);
				    	    
				    	    if (word != null && type != null && data != null) {
				    	    	try {
				    	    		ourDict.put(recToInsert);
				    	    	}
				    	    	catch (DictionaryException e) {
									// TODO Auto-generated catch block
				    	    		System.out.print(word +type+ data);
				    	    	}
				    	    }
				    	    else {
				    	    	System.out.println("Error trying to insert item" + word + " " + type + " " + data + ".");
				    	    	break;
				    	    }
				    	}
				    	i++;
				        line = br.readLine();
				    }
				    
				} 
				    catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				    catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 try {
						br.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
	}		

	
	
}
