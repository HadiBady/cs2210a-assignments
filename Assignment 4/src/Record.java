/**
 * 
 * @author hadi
 * This class Record will store the key which takes the class Pair for the keys and String for data
 */


public class Record {
	
	private String data = ""; // this will store the data, which could be filename for audio or image or text
	private Pair key; // will store the pair that stores the type and word for the data stored
	
	
	/**
	 * A constructor which initializes a new Record object with the specifed key and data.
	 * If the type in the key is "audio" or "image", then data stores the name of 
	 * the corresponding audio or image file
	 * 
	 * */
	public Record(Pair key, String data) {
		this.key = key;
		this.data = data;
	}
	
	/**
	 * @returns the key stored in this record object
	 * 
	 * */
	public Pair getKey() {
		return key;
	}
	
	/**
	 * @returns the data stored in this Record object
	 * 
	 * */
	public String getData() {
		return data;
	}
}
