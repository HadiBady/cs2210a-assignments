/**
 * 
 * @author hadi badawi
 * @about this class Pair will hold the word and type, which will represent the key for our OrderedDictionary
 * 
 */
public class Pair {
	// the word and type will hold the word and type of the variable
	private String word = "", type = "";
	
	/**
	 * The Constructor which initializes a new Pair object with the specified word and type
	 * @param  word    can be any number of letters
	 * @param  type    must be type of text, audio, or image
	 * */
	public Pair(String word, String type) {
		this.word = word.toLowerCase();
		this.type = type;
	}
	
	/**
	 * @return the word stored in this pair object
	 * */
	public String getWord() {
		return this.word;
	}
	
	
	/**
	 * @return the type stored in this pair object
	 * 
	 * */
	public String getType() {
		return this.type;
	}
	
	/**
	 * compareTo function compares a given Pair with itself
	 * @param k			the Pair that will be compared with itself
	 * @return			return an integer that will represent whether the keys are alike or not
	 */
	
	public int compareTo(Pair k) {
		  // first case if the 2 keys are equal both by word and type
		  if (this.word.equals(k.getWord()) && this.type.equals(k.getType())) {
			  return 0;
		  }
		  // second case that will compare and see if the keys both share the same workd
		  else if (this.word.equals(k.getWord())) {
			  //obtaining the lexographic value for the 2 types 
			  int thisTypeLexoVal = TypeCompareTo(this.type);
			  int kTypeLexoVal = TypeCompareTo(k.getType());
			  
			  // compare the 2 values obtained,  if the lexoVal for itself is less than the key
			  // being compared, then you return -1, otherwise return 1
			  if (thisTypeLexoVal < kTypeLexoVal) {return -1; }
			  else {return 1;}
		  }
		  // this last condition is when the words are not equal
		  else {
			  //if the word for itself is less than the value it is being compared to, then return
			  //negative one, otherwise return positive 1
			  if (this.word.compareTo(k.getWord()) > 0) {
				  return 1;
			  }
			  else  {
				  return -1;
			  }
		  }
	
	
	}
	
	
	/**
	 * private method to return the lexographic value for the each type
	 * this will aid in the CompareTo method
	 * 
	 * @param String type		this is the type of the Pair class
	 * @return					an integer
	 * 
	 * */
	private int TypeCompareTo(String type) {
		// these numbers represent 3 the largest lexographic value and 1 the lowest,
		// thus text has value 3, audio as have 1, and text has value 2
		 if (this.type.equals("text")) {
			  return 3;
		  }
		  else if (this.type.equals("audio")) {
			  return 1;
		  }
		  else  {
			  return 2;
		  }
	}
				
	
}
