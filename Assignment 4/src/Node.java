/**
 * 
 * @author hadi
 *	This Node class will store the Record object "head" which will store the the key and data for the OrderedDictionary
 *	and it will store links for the parent, left, and right child. 
 */


public class Node {

	private Record head; // head will store the key and data in this Record type
	private Node leftChild, rightChild , parent = null; // will store the links 
	
	/**
	 * The Construct takes in a Record and sets it as its head
	 * @param newRec	will be the Record that will be stored in this Node
	 * the left and right child are both set to null
	 */
	public Node(Record newRec) {
		this.head = newRec;
		this.leftChild = null;
		this.rightChild = null;
	}

	/**
	 * Function getHead returns the head of this node, hence, the record of this Node
	 * @return the Record stored in this class
	 */
	public Record getHead() {
		return head;
	}

	/**
	 * Function setHead overrides the current Record with a new Record
	 * @param head	is the Record that will replace the current head in this class
	 */
	public void setHead(Record head) {
		this.head = head;
	}

	/**
	 * Function getLeftChild returns the left child of this Node
	 * @return 	the current left child of the current node
	 */
	public Node getLeftChild() {
		return leftChild;
	}

	/**
	 * Function setleftChild overrides the current left child for this Node
	 * @param leftChild		is the left child that will override the current left child 
	 */
	public void setLeftChild(Node leftChild) {
		this.leftChild = leftChild;
	}

	/**
	 * Function getRightChild returns the right child of this node
	 * @return	the current right child of thi current node
	 */
	public Node getRightChild() {
		return rightChild;
	}

	/**
	 * Function setRightChild overrides the current right child for this Node
	 * @param rightChild	is the right child that will override the current right child
	 */
	public void setRightChild(Node rightChild) {
		this.rightChild = rightChild;
	}

	/**
	 * Function getParent returns the current parent of this Node
	 * @return the current parent of this node
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * Function setParent() overrides the current parent for this node
	 * @param parent	is the parent that will override the current parent
	 */
	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	
	
}
