/**
 * 
 * @author hadi
 *	The OrderedDictionary implements the OrderedDictionaryADT, which will use the Node, Record, and the Pair classes
 *	in order to store the items into a binary search tree, with the leaves stored as empty nodes, and the internal nodes store data
 *	the data could hold a picture, a wave file, or text
 */


public class OrderedDictionary implements OrderedDictionaryADT {
	
	
	private Node root; // this will store the root for this OrderedDictionary

	/**
	 * Constructor simply sets the root to the value null
	 * the root will hold the entire binary search tree
	 */
	public OrderedDictionary() {
		this.root = null; //coded my algorithm so that the root is null when it contains nothing
	}
	
	
	/**
	 *  Function get returns the Record object with key k, or it returns null if such 
	 *  a record is not in the dictionary. 
	 * @param k			contains the Pair what we want to find in the OrderedDictionary
	 * @return			the Record found that matches the k given, otherwise, it returns null if
	 * 					the item is not found.
	 */
	@Override
	public Record get(Pair k) {
		// using the getHelper() function to see if the item is in the tree
		Node temp = getHelper(k, this.root);

		// if temp is not null, and its root is not null, and the key is not null 
		if (temp != null && temp.getHead() != null && temp.getHead().getKey() != null) {
			// if the temp is equal to k, then return the Record stored in the Node temp
			if (temp.getHead().getKey().compareTo(k) == 0){
				return temp.getHead();	
			}
		}
		// for all the other cases, return null since the item was not found
		return null;
	}
	
	/**
	 * Function put inserts r into the ordered dictionary. 
	 * @throws DictionaryException when you try to put an item into the OrderedDictionary that is already
	 * 								present 
	 * @param	r 		is the Record that the user wishes to add to the OrderedDictionary
	 */
	@Override
	public void put(Record r) throws DictionaryException{
		// using the getHelper to see if the item is already present in the OrderedDictionary
		Node tempRec = getHelper(r.getKey(), root);
		
		
		if (tempRec != null) {
			// if the item is already present present, we have to make sure it matches, it is does then we have
			// to throw the DictionaryException()
			if (tempRec.getHead() != null && tempRec.getHead().getKey() != null && tempRec.getHead().getKey().compareTo(r.getKey())==0 )
				throw new DictionaryException("put");
			// Otherwise, we need to add this Record r to the OrderedDictionary
			else {
					// set the current tempRec's root to r since this algorithm already has
					// empty leave nodes that could be recycled to internal nodes with data
					tempRec.setHead(r);
					
					// now creating new null node that will be set to be the right child of the tempRec
					// and making sure that its parent is set to tempRec
					Record newRec = null;
					Node node1 = new Node(newRec);
					tempRec.setLeftChild(node1);
					node1.setParent(tempRec);
					
					// now doing the same thing for the left child
					Record newRec2 = null;
					Node node2 = new Node(newRec2);
					tempRec.setRightChild(node2);
					node2.setParent(tempRec);
				
				}
			}
		// this is the condition when the root is null, meaning that this is the first item being added
		// to the OrderedDictionary!!
		if (root == null) {
			// set the root to a new Node having r as the Record
			root = new Node(r);
			
			// now repeating the same steps where a null node that will be set to be a child of the root
			// and making sure that its parent is set to root. Then repeat the steps to the other child
			Record newRec = null;
			Record newRec2 = null;
			Node node1 = new Node(newRec);
			Node node2 = new Node(newRec2);
			root.setLeftChild(node1);
			root.setRightChild(node2);
			node1.setParent(root);
			node2.setParent(root);
		}
	}

	/**
	 * Function remove takes in k and attempts to remove the item from the OrderedDictionary 
	 * @param k 		is a Pair the represents the item that is desired to be removed from the OrderedDictionary
	 * @throws DictionaryException if the record is not in the dictionary
	 */
	@Override
	public void remove(Pair k) throws DictionaryException{
		// using the removeHelper to remove the item
		boolean result = removeHelper(root, k);
		// if the item already is not present, the removeHelper returns false,
		// which makes this statement true, thus throwing the exception
		if (!result) {
			throw new DictionaryException("remove");
		}
	}

	/**
	 * Function successor finds the successor and returns it
	 * @param k 	a Pair that does not have to be in the dictionary
	 * @return 		the successor of k (the record from the ordered dictionary with
	 * 				smallest key larger than k); it returns null if the given key has 
	 * 				no successor.  
	 */
	public Record successor(Pair k) {
		// if the OrderedDictionary is empty or the root is a leaf, then return null
		// which indicates that there is no successor for this pair
		if (isLeaf(root) || root == null) {
			return null;
		}
		// this case deals with the other cases, where the OrderedDictionary has nodes
		// that are storing values in it
		else {
			// using the gethelper to find out if the item is not in the OrderedDictionary
			Node successor = getHelper(k, root); 
			// if you get something that is not null, then you know that you got a node
			if (successor !=null) {
				// if the successor is an internal node, then we have to find the smallest of its
				// right child, and return that node
				if (!isLeaf(successor) && !isLeaf(successor.getRightChild())) {
					return smallestHelper(successor.getRightChild()).getHead();
				}
				// this is the case where you have the successor be a leaf, which means
				// that the item could have a successor or not, depending on where the 
				// leaf is.
				else {
					// grabbing the parent of the successor
					Node parent = successor.getParent();
					//if the parent is not null, meaning not root
					if (parent != null) {
						// while loop to keep going up as long as the successor is always larger
						// then its parent
						while(successor != root && successor == parent.getRightChild()) {
							successor = parent;
							parent = successor.getParent();
						}
						// if you end up back at the root, then the successor does not exist
						// because the value you searched with was the largest or larger than the 
						// largest item in the OrderedDicitonary, thus return null
						if (successor == root) {return null;}
						// in this case, you did end up finding a value that is larger than k, thus
						// return parent, which represents that larger value then k
						else {return parent.getHead();}
					}
				}
			}
		}
		// return null since in this case, you did not have any items in the OrderedDictionary
		return null;
	}

	/**
	 * Function predecessor finds the predecessor and returns it
	 * @param k		a pair that does not have to be in the OrderedDictionary
	 * @return 		the predecessor of k (the record from the ordered dictionary 
     * 				with largest key smaller than k; it returns null if the given
     * 				key has no predecessor. 
	 */
	@Override
	public Record predecessor(Pair k) {
		// if the OrderedDictionary is empty or the root is a leaf, then return null
		// which indicates that there is no predecessor for this pair
		if (isLeaf(root) || root == null)
			return null; 
		// this case deals with the other cases, where the OrderedDictionary has nodes
		// that are storing values in it
		else {
			// using the gethelper to find out if the item is not in the OrderedDictionary
			Node predecessor = getHelper(k, root);
			// if you get something that is not null, then you know that you got a node
			if (predecessor !=null) {
				// if the predecessor is an internal node, then we have to find the largest of its
				// left child, and return that node
				if (!isLeaf(predecessor) && !isLeaf(predecessor.getLeftChild())) {
					return largestHelper(predecessor.getLeftChild()).getHead();
				}
				// this is the case where you have the predecessor be a leaf, which means
				// that the item could have a predecessor or not, depending on where the 
				// leaf is.
				else {
					// grabbing the parent of the predecessor
					Node parentPred = predecessor.getParent();	
					//if the parent is not null, meaning not root
					if (parentPred != null) {
						// while loop to keep going up as long as the predecessor is always smaller
						// then its parent
						while (predecessor != root  && parentPred.getLeftChild() == predecessor){
							predecessor = parentPred;
							parentPred = predecessor.getParent();
						}
						// if you end up back at the root, then the predecessor does not exist
						// because the value you searched with was the smallest or smaller than the 
						// smallest item in the OrderedDicitonary, thus return null
						if (predecessor == root) return null;
						// in this case, you did end up finding a value that is smaller than k, thus
						// return parent, which represents that smaller value then k
						else {return parentPred.getHead();}
					}
				}
			}
		}
		// return null since in this case, you did not have any items in the OrderedDictionary
		return null;
	}
	
	
	/**
	 * Function smallest finds the smallest value in the OrderedDictionary and returns that record
	 * @return 		the record with smallest key in the ordered dictionary and returns null if the 
	 * 				dictionary is empty.  
	 */
	@Override
	public Record smallest() {
		// using the smallestHelper to find the smallest item in the Dictionary
		Node small = smallestHelper(root);
		// if the item is not null (hence there is a small value), return its record
		if (small != null) {
			return small.getHead();
		}
		// otherwise, return null, indicating that the OrderedDictionary is empty
		return null;
	}

	/** 
	 * Function largest returns the largest item in the OrderedDictionary
	 * @returns 		the record with largest key in the ordered dictionary or returns null if 
	 * 					the dictionary is empty.  
	 */
	@Override
	public Record largest() {
		// the helper method returns the largest item in the OrderedDictionary
		Node large = largestHelper(root);
		// if the item is not null, then return the Record stored
		if (large != null) {
			return large.getHead();
		}
		// return null which is the case where the OrderedDictionary is empty
		return null;
	}
	
	/**
	 * Function removeHelper is a helper method that removes the nodes from the OrderedDictionary
	 * @param root		represents the root of the tree, could be the same root as the OrderedDictionary
	 * @param k			represents the parameter that we wish to remove from the tree
	 * @return			true if the item was easily removed, otherwise, return false if not exist
	 */
	private boolean removeHelper(Node root, Pair k) {
		// using the getHelper to see if the item is already present
		Node tempRec = getHelper(k, root);

		// if the tempRec is a leaf, in otherwords, the item was not found, then return false
		if (isLeaf(tempRec)) {
			return false;
		}
		
		// this deals with when you get something from the getHelper that is not a leaf, hence
		// an internal node
		else {
			// if the left child is null, but the right child is storing a/node(s)
			if (tempRec.getLeftChild() == null && tempRec.getRightChild() != null) {
				// get the right child and the parent of tempRec
				Node tempRight = tempRec.getRightChild();
				Node tempRec2 = tempRec.getParent();
				// if the tempRec is equal to the root, thus we must remove the root by setting
				// the right child as a new root and the parent of that right child is set to
				// null.
				if (tempRec == root) {
					root= tempRight;
					root.setParent(null);
				}
					
				// if tempRec is not root, then set the right child in place of tempRec,
				// and set the parent of the right child to the parent of tempRec,
				// which removes tempRec
				else {
					if (tempRec2 != null) {
					tempRec2.setRightChild(tempRight);
					tempRight.setParent(tempRec2);
					}
						
				}
			}
			
			// if the right child is null, but the left child contain(s) node(s)
			else if (tempRec.getLeftChild() != null && tempRec.getRightChild() == null) {
				// get the left child and the parent of tempRec
				Node tempLeft = tempRec.getLeftChild();
				Node tempRec2 = tempRec.getParent();
				
				// if tempRec is equal to the root, we must remove current root by setting
				// the left child as the new root and the parent of the left child to 
				// null.
				if (tempRec == root) {
					root= tempLeft;
					root.setParent(null);
				}
				// if tempRec is not root, then set the left child in place of tempRec,
				// and set the parent of the left child to the parent of tempRec
				// which removes tempRec
				else {
					tempRec2.setLeftChild(tempLeft);
					tempLeft.setParent(tempRec2);
				}
			}
			
			// this could be that either both the children are null or nodes
			else {
				// getting the smallest node from the right child of the right child
				Node smallNode = smallestHelper(tempRec.getRightChild());
				
				// Due to the way i made my previous statements, i can have the left and right
				// child be null but the previous statements may not catch them
				// so what i did is that these catch the cases where the actual
				// "head"s of the nodes are null, hence, store no data in it
				if (smallNode == null && tempRec.getLeftChild().getHead() != null) {
					// this case the left child is not null, thus we override
					// tempRec with properties of its left child
					// and make sure that the parent and left child have each other
					// so that parent is parent of left child and parent of left child
					// and left child is a child of parent
					tempRec.setHead(tempRec.getLeftChild().getHead());
					tempRec.getLeftChild().setParent(tempRec.getParent());
					if (tempRec.getParent().getLeftChild() == tempRec){
						tempRec.getParent().setLeftChild(tempRec.getLeftChild());
						}
					else {
						tempRec.getParent().setRightChild(tempRec.getLeftChild());
					}
					tempRec.setLeftChild(tempRec.getLeftChild().getLeftChild());
					tempRec.setRightChild(tempRec.getLeftChild().getRightChild());
					}
				// if both are null, then that means we are removing the last item in the this binary search tree
				// thus we set the head, left child, and right child to null
				
				else if (smallNode == null) {
					tempRec.setHead(null);
					tempRec.setLeftChild(null);
					tempRec.setRightChild(null);
					}
				
				// This is the case where both children are not null, thus we get the key
				// and the data stored, override the tempRec's key and data, and then remove
				// the smallest Node by calling the function again with the smallnode as the root
				// and the pair as key of smallNode
				else {
					Pair tempKey = smallNode.getHead().getKey();
					String tempData = smallNode.getHead().getData();
		
					tempRec.setHead(new Record(tempKey, tempData));
					removeHelper(smallNode, tempKey);
				}
				
			}
		// return true to show that the operations were successful in the else condition
		return true;
		}
	}
				
	/**
	 * Function getHelper takes in 2 paramters and returns the Node 
	 * @param k		represents the k which we compare to see if the item is in the OrderedDictionary
	 * @param node	node actually represents the node of the root of the tree
	 * @return		the Node which stores if the item is found, otherwise, returns the node that is empty
	 * 				if it could not find that item
	 */
	private Node getHelper(Pair k, Node node) {
		// holding the node in the temporary temp variable
		Node temp = node;
		
		// if the node given is not null
		if (temp != null && temp.getHead() !=null) {
			// if the node given is a leaf, thus there are no children to go through, thus return
			// temp
			if (node.getLeftChild() == null && node.getRightChild() == null && node.getHead() == null) {
				return temp;
				}
			// condition for the other cases where temp is not null and may have 1 child that is storing something
			else {
				// to make sure that the key values actually exist to perform this comparisons
				if (temp.getHead().getKey() != null) {
					// if the K desired is already in the node, then return temp
					if ((temp.getHead().getKey().compareTo(k)) == 0) {
						return temp;
					}
					// if the k desired is greater than node, then call the function recursively
					// to find the desired value on the left child, if the left child is null,
					// then make an empty node and return that (this indicates a new location
					// to which you can add a node)
					else if ((temp.getHead().getKey().compareTo(k)) > 0) {
						if (temp.getLeftChild() != null) {
							return getHelper(k, temp.getLeftChild());
						}
						Node tempNode = new Node(null);
						tempNode.setParent(temp);
						
						temp.setLeftChild(tempNode);
						
						return tempNode;
					}
					// if the k desired is less than node, then call the function recursively
					// to find the desired value on the right child, if the right child is null,
					// then make an empty node and return that (this indicates a new location
					// to which you can add a node)
					else {
						if (temp.getRightChild() != null) {
							return getHelper(k,temp.getRightChild());
						}
						
						Node tempNode =new Node(null);
						tempNode.setParent(temp);
						temp.setRightChild(tempNode);
						
						return tempNode;
						
					}
				}
				}
			
			}
		// this value that is returned is the null one, which indicates that there is nothing in the OrderedDictionary
		return temp;
	}
	
	/**
	 * Function smallestHelper  does the work to find the smallest and returns that
	 * @param r		is the Node that represents the root of a tree binary tree
	 * @return		r if the children are null, null if there are no nodes in that binary search tree,
	 * 				or the smallest value of the tree
	 */
	private Node smallestHelper(Node r) {
		// return null if the ordered dictionary is empty or the head is empty
		if (r == null || r.getHead() == null) {
			return null;
		}
		// return r if it is the only item in that binary search tree
		if (r.getLeftChild() == null && r.getRightChild() == null) {
			return r;
		}
		// case where there are multiple nodes in the binary search tree
		else {
			// temp variable that will help us traverse
			Node p = r;
			// to keep going down until you reach a node that is a leaf
			while (!isLeaf(p)) {
				p = p.getLeftChild();		
			}
			// return that parent of that leaf, which is the smallest value
			return p.getParent();
		}
	}
	
	/**
	 *  Function largestHelper finds the largest node in the given binary search tree
	 * @param r		represents a binary search tree
	 * @return		null if the binary search tree is empty, returns 
	 */
	private Node largestHelper(Node r) {
		// return null if the ordered dictionary is empty or the head is empty
		if (r == null || r.getHead() == null) {
			return null;
		}
		
		// return r if it is the only item in that binary search tree
		if (r.getLeftChild() == null && r.getRightChild() == null) {
			return r;
		}
		
		// case where there are multiple nodes in the binary search tree
		else {
			// temp variable that will help us traverse
			Node p = r;
			// to keep going down until you reach a node that is a leaf
			while(!isLeaf(p)) {
				p = p.getRightChild();
			}
			// return that parent of that leaf, which is the largest value
			return p.getParent();
		}
	}
	
	/**
	 * Function isLeaf is a helper function to determine if a given node is a leaf
	 * @param k		is the node that we wish to test
	 * @return		true if k is a node, and false otherwise
	 */
	private boolean isLeaf(Node k) {
		return k.getLeftChild() == null && k.getRightChild() == null && k.getHead() == null;
	}
	
}
